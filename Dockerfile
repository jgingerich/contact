FROM openjdk:8-jre
RUN mkdir -p /var/lib/app
WORKDIR /var/lib/app
ADD build/libs/contact.jar contact.jar
EXPOSE 8080
ENTRYPOINT java -jar contact.jar
