package com.docmos.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

    @PostMapping("/contact")
    public void contact(@RequestBody ContactMessage message) {
        LOGGER.info("Received contact message with name={}, email={}, message={}", message.getName(), message.getEmail(), message.getMessage());
    }

}
