$(function() {
    $("#view-sent").hide();
    $("#contact-submit").on("click", function(e) {
        $.ajax("api/contact", {
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                name: $("#contact-name").val(),
                email: $("#contact-email").val(),
                message: $("#contact-message").val()
            }),
            success: function() {
                $("#view-contact").hide();
                $("#view-sent").show();
            }
        });
    });
});